#include "Table.h"

Table::Table() :capacite_(MAXCAP), nbPlats_(0), id_(-1), nbPlaces_(1), occupee_(false) {
	commande_ = (new Plat*[capacite_]());
};

Table::Table(int id, int nbPlaces) :capacite_(MAXCAP), nbPlats_(0), id_(id), nbPlaces_(nbPlaces), occupee_(false) {
	commande_ = (new Plat*[capacite_]());

};

// La fonction getId est constante et � pour but de retourner le id d'une table.
int Table::getId() const {
	return id_;
};

// La fonction getNbPlaces est constante et � pour but de retourner le nombre de place d'une table.
int Table::getNbPlaces() const {
	return nbPlaces_;
};

// La fonction estOccupee est constante et � pour but de retourner vrai ou faux d�pendemment de la condition de la table.
bool Table::estOccupee() const {
	return occupee_;
};

// La fonction libererTable � pour but de reinitialiser les pointeurs de Plats d'une commande ainsi que delete cette commande et 
// rendre la table non occup�e.
void Table::libererTable() {
	for (int i = 0; i < nbPlats_; i++)
		commande_[i] = nullptr;
	delete[] commande_;
	commande_ = nullptr;
	occupee_ = false;
};

// La fonction placerClient a pour but d'indiquer que la table est occup�e.
void Table::placerClient() {
	occupee_ = true;
};

// La fonction setId prend en param�tre un id de table et � pour but de fixer ce id.
void Table::setId(int id) {
	id_ = id;
};

// La fonction commander prend en param�tre le pointeur d'un plat et � pour but de placer la commande en m�me temps d'augmenter le
// nombre de plats.
void Table::commander(Plat* plat) {
	commande_[nbPlats_] = plat;
	nbPlats_++;
	
};

// La fonction getChiffreAffaire � pour but de d�terminer le profit du magasin en additionnant tous les gains et en enlevant de 
// ce montant les pertes (cout du plat).
double Table::getChiffreAffaire() {
	double chiffreAffaire = 0.0, gain = 0.0, perte = 0.0;
	for (int i = 0; i < nbPlats_; i++) {
		if (commande_[i] != nullptr) {
			gain += commande_[i]->getPrix();
			perte += commande_[i]->getCout();
		}
	}

	chiffreAffaire = gain - perte;
	return chiffreAffaire;
};

// La fonction afficher � pour but d'afficher la condition de la table (occup�e ou pas) ainsi que la commande passer par les clients
// � cette table.
void Table::afficher() {

	if (occupee_) {
		if (nbPlats_ > 0) {
			cout << "      La table numero " << id_ << " est occupee. Voici la commande passee par les clients :" << endl;
		}
		else
			cout << "      La table numero " << id_ << " est occupee. Or, il n'y a pas de commande en ce moment." << endl;

		for (int i = 0; i < nbPlats_; i++) {
			if (commande_[i] != nullptr)
				commande_[i]->afficher();
			else
				cout << "      L'aliment demande n'a pas ete trouve." << endl;
		}
		cout << endl;
	}
	else
		cout << "      La table numero " << id_ << " est libre." << endl << endl;


};