#include "Plat.h"



Plat::Plat() :nom_("inconnu"), prix_(0.0), cout_(0.0) {
};

Plat::Plat(string nom, double prix, double cout) :nom_(nom), prix_(prix), cout_(cout) {
};

// La fonction getNom est constante et � pour but de retourner le nom d'un plat.
string Plat::getNom() const {
	return nom_;
};

// La fonction getPrix est constante et � pour but de retourner le prix d'un plat.
double Plat::getPrix() const {
	return prix_;
};

// La fonction getCout est constante et � pour but de retourner le cout pour le restaurant d'un plat.
double Plat::getCout() const {
	return cout_;
};

// La fonction setNom prend en param�tre un nom de plat et fixe ce nom au Plat.
void Plat::setNom(string nom) {
	nom_ = nom;
};

// La fonction setPrix prend en param�tre un prix et fixe ce prix pour un Plat.
void Plat::setPrix(double prix) {
	prix_ = prix;
};

// La focntion setCout prend en param�tre le cout et fixe ce cout pour un Plat. 
void Plat::setCout(double cout) {
	cout_ = cout;
};

// La focntion afficher � pour but d'afficher le nom, le prix ainsi quue le cout d'un Plat.
void Plat::afficher() {
	cout << "      " << nom_ << " - " << prix_ << " $ (" << cout_ << "$ pour le restaurant)" << endl;
};

