#include "Menu.h"

Menu::Menu() :capacite_(MAXPLAT), nbPlats_(0), type_(Matin) {
	listePlats_ = new Plat*[capacite_]();

};

Menu::Menu(string fichier, TypeMenu type) :capacite_(MAXPLAT), nbPlats_(0), type_(type) {
	listePlats_ = new Plat*[capacite_]();
	lireMenu(fichier);

};

// La fonction getNbPlats est constante et � pour fonction de retourner le nombre de plats dans un menu. 
int Menu::getNbPlats() const {
	return nbPlats_;
};

// La fonction afficher affiche tous les plats dans un menu d�pendamment du Moment de la journ�e (matin, midi ou soir)  
void Menu::afficher() {
	switch (type_) {
	case 0: cout << "Matin :" << endl; break;
	case 1: cout << "Midi :" << endl; break;
	case 2: cout << "Soir :" << endl; break;
	}
	for (int i = 0; i < nbPlats_; i++)
		listePlats_[i]->afficher();
	cout << endl;
};

// La fonction trouverPlat prend en param�tre un nom de plat et retourne un pointeur vers le plat avec le m�me nom ou un pointeur null si le plat n'est pas trouv�.
Plat* Menu::trouverPlat(string& nom) {
	string nomPlat = " ";
	for (int i = 0; i < nbPlats_; i++) {
		nomPlat = listePlats_[i]->getNom();
		if (nom == nomPlat)
			return listePlats_[i];
	}
	return nullptr;
};

// La fonction ajouterPlat prend en param�tre un Plat et ajoute celui-ci dans la liste de plat en m�me temps d'augmenter le nombre de plats
void Menu::ajouterPlat(Plat & plat) {
	*listePlats_[nbPlats_] = plat;
	nbPlats_++;
};

// La fonction ajouterPlat prend en param�tres le nom du plat, le montant ainsi que le co�t pour le restaurant. Elle a pour fonction 
// de fixer tout ces param�tres dans la liste de plats et augmente le nombre de plats.
void Menu::ajouterPlat(string& nom, double montant, double cout) {
	listePlats_[nbPlats_]->setNom(nom);
	listePlats_[nbPlats_]->setPrix(montant);
	listePlats_[nbPlats_]->setCout(cout);

	nbPlats_++;
};

// La fonction lireMenu prend en param�tre un nom de fichier et � pour fonction de lire les plats pour un certain moment de 
// la journ�e et ajoute les plats dans la liste de plats.
bool Menu::lireMenu(string& fichier) {
	ifstream entree(fichier);
	string typeVoulu = " ", typeLu = " ", prochainType = " ", nomTemporaire = " ";
	int i = 0;
	double prix = 0.0, cout = 0.0;
	switch (type_) {
	case (Matin): typeVoulu = "-MATIN"; prochainType = "-MIDI"; break;
	case (Midi): typeVoulu = "-MIDI"; prochainType = "-SOIR"; break;
	case (Soir): typeVoulu = "-SOIR"; prochainType = "-TABLES"; break;
	}

	while (!ws(entree).eof()) {
		entree >> typeLu;
		if (typeLu == typeVoulu) {
			entree >> nomTemporaire;
			do {
				entree >> prix;
				entree >> cout;
				Plat* plat = new Plat(nomTemporaire, prix, cout);
				listePlats_[i] = plat;
				entree >> nomTemporaire;
				i++;
				nbPlats_++;
			} while (nomTemporaire != prochainType);

		}
		else
			entree.ignore(80, '\n'); // ignore si ce n'est pas le bon Moment de la journ�e
	}
	return true;
};