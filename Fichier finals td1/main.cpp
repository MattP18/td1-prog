/*
* Titre : main.cpp - Travail Pratique #1
* Date : 09 Janvier 2019
* Auteur : David Dratwa
*/

#include "Restaurant.h"

using namespace std;

// R�ponse aux questions 
/*
#1:
Le lien entre les classes Menu et Plat est agr�gation par pointeur. Effectivement, on utilise l'objet plat dans l'objet menu, toutefois,
c'est une relation d'aggr�gation puisque l'objet plat ne dispara�t pas lorsque l'objet englobant est d�truit qui est dans ce cas
Menu. De plus, Plat est pass� par pointeur, c'est pour ces raisons que le lien est agr�gation par pointeur. 


#2:
Si une m�thode est constante, ceci aura pour effet que la m�thode ne peut effectuer aucune modification sur les attributs de
l'objet en question. Mettre const dans un m�thode est particuli�rement inter�ssant l'orsqu'on ne veut pas acc�der aux attributs 
d'un objet. Effectivement, dans les getters, on ne veut pas modifier les valeurs mais plut�t obtenir ces valeurs, c'est pour cette 
raison qu'il �tait pertinent d'ajouter const.


*/

int main() {

	//creation du restaurant - avec le fichier donne - le nom : PolyFood - moment de la journee : soir. 
	string fichier = "polyFood.txt";
	string nom = "polyFood";

	Restaurant restaurant(fichier, nom, Soir);

	//creer plusieurs clients -- des entiers 

	int client1 = 1;
	int client2 = 5;
	int client3 = 15;
	int client4 = 3;
	int client5 = 2;

	//placer les clients 
	restaurant.placerClients(client1);
	restaurant.placerClients(client2);
	restaurant.placerClients(client3);
	restaurant.placerClients(client4);
	restaurant.placerClients(client5);

	// commander des plats
	//Poisson - Table 1 
	string poisson = "Poisson";
	restaurant.commanderPlat(poisson, 1);
	//Poulet - Table 2 
	string poulet = "Poulet";
	restaurant.commanderPlat(poulet, 2);
	//Pizza - Table 2 
	string pizza = "Pizza";
	restaurant.commanderPlat(pizza, 2);
	//Poulet - Table 4
	restaurant.commanderPlat(poulet, 4);
	//Muffin - Table 4 
	string muffin = "Muffin";
	restaurant.commanderPlat(muffin, 4);
	//Oeud - Table 4 
	string oeud = "Oeud";
	restaurant.commanderPlat(oeud, 4);


	//afficher le restaurant
	restaurant.afficher();

	cout << "-------------------------------------------------" << endl;

	//liberer les tables 

	restaurant.libererTable(1);
	restaurant.libererTable(2);
	restaurant.libererTable(3);
	restaurant.libererTable(4);

	//afficher le restaurant 
	restaurant.afficher();

	return 0;

}