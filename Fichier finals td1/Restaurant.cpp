#include "Restaurant.h"

Restaurant::Restaurant() :nom_(new string("inconnu")), chiffreAffaire_(0.0), momentJournee_(Matin), capaciteTables_(MAXCAP), nbTables_(0){
	menuMatin_ = new Menu();
	menuMidi_ = new Menu();
	menuSoir_ = new Menu();
	tables_ = new Table*[capaciteTables_]();



};

Restaurant::Restaurant(string& fichier, string& nom, TypeMenu moment) :nom_(new string(nom)), chiffreAffaire_(0.0), momentJournee_(moment), capaciteTables_(MAXCAP), nbTables_(0){
	menuMatin_ = new Menu(fichier, Matin);
	menuMidi_ = new Menu(fichier, Midi);
	menuSoir_ = new Menu(fichier, Soir);
	tables_ = new Table*[capaciteTables_]();
	lireTable(fichier);
};

// La fonction setMoment prend en param�tre un moment de la journ�e et fixe ce moment. 
void Restaurant::setMoment(TypeMenu moment) {
	momentJournee_ = moment;
};

// La fonction getNom est constante et � pour but de retourner le nom du restaurant. 
string Restaurant::getNom() const {
	return *nom_;
};

// La fonction getMoment est constante et � pour but de retourner le moment de la journ�e.
TypeMenu Restaurant::getMoment() const {
	return momentJournee_;
};

// La fonction lireTable prend en param�tre un nom de fichier et � pour fonction de lire le id d'une d'une table ainsi
// que le nombre de places d'une table et ajouter cette table gr�ce � la fonction ajouterTable.
void Restaurant::lireTable(string& fichier) {
	ifstream entree(fichier);
	string typeVoulu = "-TABLES", typeLu = " ";
	int id = 0, nbPlaces = 0;
	while (!ws(entree).eof()) {
		entree >> typeLu;
		if (typeVoulu == typeLu) {
			while (!ws(entree).eof()) {
				entree >> id;
				entree >> nbPlaces;
				ajouterTable(id, nbPlaces);
			}
		}
		else
			entree.ignore(80, '\n'); // ignore si on ne lit pas une table
	}
};

// La fonction ajouterTable prend en param�tre le id et le nombre de place d'une table et ajouter cette table aux tables d�j� existante.
void Restaurant::ajouterTable(int id, int nbPlaces) {
	Table* table = new Table(id, nbPlaces);
	tables_[nbTables_++] = table;


};

// La fonction libererTable prend en param�tre le id d'une table et fait app�le � la fonction libererTable de Table. Toutefois, avant 
// de lib�rer la Table, cette fonction ajoute le montant fait par la table dans le chiffre d'affaire du restaurant.
void Restaurant::libererTable(int id) {
	chiffreAffaire_ += tables_[id - 1]->getChiffreAffaire();
	tables_[id - 1]->libererTable();

};

// La fonction commandePlat prend en param�tre le nom et le id d'une table et cr�e un nouveau pointeur de Plat d�pendamment du moment
// de la journ�e afin de commander le Plat.
void Restaurant::commanderPlat(string& nom, int idTable) {
	Plat* ptr = new Plat();
	switch (momentJournee_) {
	case 0: ptr = menuMatin_->trouverPlat(nom); break;
	case 1: ptr = menuMidi_->trouverPlat(nom); break;
	case 2: ptr = menuSoir_->trouverPlat(nom); break;

	}
	tables_[idTable - 1]->commander(ptr);
};

// La fonction placerClients prend en param�tres de nbClients � placer � une table. Elle a pour fonction de trouver une table de libre 
// de mani�re la plus efficace en r�duisant le nombre de place vide � une table. Si toutes les tables sont occup�es, cette fonction 
// affiche un message d'erreur.
void Restaurant::placerClients(int nbClients) {
	int nbTablesOccupee = 0, difference = 0, differenceMin = 6, indiceMin = 0;
	for (int i = 0; i < nbTables_; ++i) {
		if (tables_[i]->estOccupee())
			nbTablesOccupee++;
		else {
			difference = (tables_[i]->getNbPlaces()) - nbClients;
			if (difference <= differenceMin && difference >= 0) {
				indiceMin = i;
				differenceMin = difference;
			}
		}
	}
	if (nbTablesOccupee == nbTables_ || differenceMin == 6)
		cout << "Erreur : il n'y a plus de tables disponibles pour le groupe de " << nbClients << " personnes." << endl << endl; 

	else
		tables_[indiceMin]->placerClient();


};

// La fonction afficher � pour but d'afficher le chiffre d'affaire du restaurant ainsi que les tables et les menus pour tous les 
// moment de la journ�e.
void Restaurant::afficher() {
	cout << "Le restaurant PolyFood a fait un chiffre d'affaire de : " << chiffreAffaire_ << "$" << endl;
	cout << "-Voici les tables :" << endl;
	for (int i = 0; i < nbTables_; ++i)
		tables_[i]->afficher();

	cout << "-Voici son menu :" << endl;
	menuMatin_->afficher();
	menuMidi_->afficher();
	menuSoir_->afficher();


};